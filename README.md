﻿# Wipro Digital Web Crawler Utility

## About



### Author

Ryan E. Anderson

---

### Description

This utility can be used to efficiently crawl a website to gather links and produce the structure of a site.

This solution contains six projects: WiproDigital.Utility, WiproDigital.WebCrawler, WiproDigital.WebCrawler.Abstractions, WiproDigital.WebCrawler.Test, WiproDigital.WebCrawler.UI, and WiproDigital.WebCrawler.UI.Control.

The web crawler works by repeatedly invoking a method to gather all nested links based on an initial set from the first page of a provided URL.  

---

### Technologies

- ASP.NET Core 2.0
- C#
- ASP.NET Core MVC
- xUnit
- Moq
- Bootstrap 3.3.7
- HTML
- CSS
- Razor
- Bash
- Markdown
- Git
- Json.NET

### Design Patterns

- Utility
- Lazy Loading
- Singleton
- Dependency Injection

### Architectural Patterns

- n-Layer
- n-Tier
- MVC
- MVVM

---

### Version

1.2.0

---

### License

None

---

## Instructions for Evaluating the Web Crawler Utility

### General Setup

Change to the WiproDigital directory, and open WiproDigital.sln.

### End-to-End Testing

1. First, ensure that the WiproDigital.WebCrawler.UI project is set as the StartUp project.
2. Run the program to launch the WebCrawler site.
3. Enter a URL.
4. Press the "Crawl" button.

### Integration/Unit Testing

1. Copy the WiproDigital.WebCrawler.UI.deps file from the bin directory of WiproDigital.WebCrawler.UI to that of WiproDigital.WebCrawler.Test.
2. In Visual Studio, click on Test from the menu bar, and select Run All Tests.
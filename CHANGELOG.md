# Change Log

## v1.1.0 - 2018-10-11

### Added
- Add a private helper to the crawler utility so that functionality for collecting all links can be reused
- Use string interpolation to join the base server address to each request URI so that links will function
- Add markup to include more details in the result

### Changed
- Bump version numbers
- Change the headers in the about page
- Change the headers in the README file
- Refactor the implementation of the web crawler by utilizing a class constant for the maximum number of links

## v1.2.0 - 2018-10-12

### Added
- Create a view component for pagination
- Create a new project and class for a pagination control
- Create a view model for pagination
- Add markup for pagination

### Changed
- Bump version numbers
- Revise controller logic for the pagination of links by utilizing temporary data and a redirect
- Add a property for storing the current page index of links
- Refactor unit tests
- Decrease the maximum number of links
- Improve the robustness of the method that returns groups of hash sets
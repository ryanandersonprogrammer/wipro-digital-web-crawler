﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace WiproDigital.WebCrawler.Abstractions
{
    /// <summary>
    /// This interface is a contract for web crawling.
    /// </summary>
    public interface IWebCrawler
    {
        /// <summary>
        /// This method can be used to crawl all pages at a provided server address.
        /// </summary>
        /// <param name="url">This is the base server address of a website.</param>
        /// <returns>This method returns a task of a hash set containing links.</returns>
        Task<HashSet<string>> Crawl(string url, HttpClient client = null);

        /// <summary>
        /// This method uses a provided set of links to build a site structure.
        /// </summary>
        /// <param name="links">This is a set of links.</param>
        /// <returns>This method returns a hash set of link groupings.</returns>
        HashSet<HashSet<string>> GetSiteStructure(HashSet<string> links);
    }
}

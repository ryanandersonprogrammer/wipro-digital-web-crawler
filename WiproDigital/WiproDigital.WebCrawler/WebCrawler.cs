﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using WiproDigital.WebCrawler.Abstractions;
using WiproDigital.WebCrawler.Utility.Web;

namespace WiproDigital.WebCrawler
{
    /// <summary>
    /// This class can be used as a basic implementation of a singleton for crawling websites and generating site structures.
    /// </summary>
    public sealed class WebCrawler : IWebCrawler
    {
        private static readonly Lazy<IWebCrawler> _instance = new Lazy<IWebCrawler>(() => new WebCrawler());

        private const int MaximumNumberOfLinks = 500;

        private WebCrawler()
        {

        }

        /// <summary>
        /// This is a single instance of a web crawler.
        /// </summary>
        public static IWebCrawler Instance => _instance.Value;

        /// <summary>
        /// This method can be used to crawl all pages at a provided server address.
        /// </summary>
        /// <param name="url">This is the base server address of a website.</param>
        /// <returns>This method returns a task of a hash set containing links.</returns>
        public async Task<HashSet<string>> Crawl(string url, HttpClient client = null)
        {
            if (client == null)
                return await WebCrawlerUtility.GetAllLinks(url, MaximumNumberOfLinks);

            return await WebCrawlerUtility.GetAllLinks(url, MaximumNumberOfLinks, client);
        }

        /// <summary>
        /// This method uses a provided set of links to build a site structure.
        /// </summary>
        /// <param name="links">This is a set of links.</param>
        /// <returns>This method returns a hash set of link groupings.</returns>
        public HashSet<HashSet<string>> GetSiteStructure(HashSet<string> links)
        {
            if (links == null || links.Count == 0)
                return null;

            var linkGroups = new HashSet<HashSet<string>>();
            var linkGroup = new HashSet<string>();

            var hashSetArray = links.ToArray();

            Array.Sort(hashSetArray);

            linkGroups.Add(linkGroup);

            var link = hashSetArray[0];
            var tokens = link.Split('/');
            var nextToken = tokens.Count() > 1 ? tokens[1] : tokens[0];

            linkGroup.Add(link);

            for (int i = 1; i < hashSetArray.Length; i++)
            {
                link = hashSetArray[i];
                tokens = link.Split('/');

                var linkToken = tokens.Count() > 1 ? tokens[1] : tokens[0];

                if (!linkToken.Equals(nextToken))
                {
                    linkGroup = new HashSet<string>();

                    linkGroups.Add(linkGroup);

                    nextToken = linkToken;
                }

                linkGroup.Add(link);
            }

            return linkGroups;
        }
    }
}

﻿using System;

namespace WiproDigital.WebCrawler.UI.Control
{
    public class Pagination
    {
        public Pagination(int totalItems, int? page, int pageSize = 10)
        {
            TotalItems = totalItems;
            PageSize = pageSize;
            TotalPages = (int)Math.Ceiling((decimal)TotalItems / PageSize);
            CurrentPage = page != null ? (int)page : 1;
            StartPage = CurrentPage - 1;
            EndPage = CurrentPage + 1;

            if (StartPage <= 0)
            {
                EndPage -= (StartPage - 1);

                StartPage = 1;
            }

            if (EndPage > TotalPages)
            {
                EndPage = TotalPages;

                if (EndPage > pageSize)
                    StartPage = EndPage - pageSize - 1;
            }
        }

        public int TotalItems { get; }
        public int CurrentPage { get; }
        public int PageSize { get; }
        public int TotalPages { get; }
        public int StartPage { get; }
        public int EndPage { get; }
    }
}

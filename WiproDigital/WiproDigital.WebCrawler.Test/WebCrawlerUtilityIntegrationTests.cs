using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.PlatformAbstractions;
using System;
using System.IO;
using System.Threading.Tasks;
using WiproDigital.WebCrawler.UI;
using WiproDigital.WebCrawler.Utility.Web;
using Xunit;

namespace WiproDigital.WebCrawler.Test
{
    public class WebCrawlerUtilityIntegrationTests
    {
        private const string StaticSamplePathAbsoluteLinks = "StaticSampleAbsolutePath.html";
        private const string StaticSamplePathFragmentLinks = "StaticSampleFragment.html";
        private const string StaticSamplePathQueryLinks = "StaticSampleQuery.html";
        private const string StaticSamplePathRelativeLinks = "StaticSampleRelativePath.html";
        private const string StaticSamplePathSubdomainLinks = "StaticSampleSubdomain.html";
        private const string StaticFileDirectoryPath = "html";
        private const string TestServerUrl = "http://localhost:5001";

        private IWebHostBuilder _builder;

        public WebCrawlerUtilityIntegrationTests()
        {
            var applicationContentRoot = Path.Combine(PlatformServices.Default.Application.ApplicationBasePath, @"..\..\..\..\WiproDigital.WebCrawler.UI");

            _builder = new WebHostBuilder()
                .UseContentRoot(applicationContentRoot)
                .UseStartup<Startup>()
                .UseUrls(TestServerUrl)
                .UseKestrel();
        }

        [Fact]
        public async Task GetLinksOptimized_ReturnsThreeOutOfThreeLinks_WhenStaticHtmlPageContainingAbsoluteLinksIsCrawled()
        {
            var expectedLinks = new string[] { "/B/BB/BBB/StaticSampleAbsolutePathB.html", "/StaticSampleAbsolutePathA.html", "/StaticSampleAbsolutePathC.html" };

            await ExecuteGetLinksOptimizedIntegrationTest(expectedLinks, StaticSamplePathAbsoluteLinks);
        }

        [Fact]
        public async Task GetLinksOptimized_ReturnsThreeOutOfFourLinks_WhenStaticHtmlPageContainingFragmentLinksIsCrawled()
        {
            var expectedLinks = new string[] { "/StaticSampleFragmentA.html", "/StaticSampleFragmentB.html", "/StaticSampleFragmentD.html" };

            await ExecuteGetLinksOptimizedIntegrationTest(expectedLinks, StaticSamplePathFragmentLinks);
        }

        [Fact]
        public async Task GetLinksOptimized_ReturnsThreeOutOfThreeLinks_WhenStaticHtmlPageContainingQueryLinksIsCrawled()
        {
            var expectedLinks = new string[] { "/StaticSampleQueryA.html", "/StaticSampleQueryB.html", "StaticSampleQueryC.html" };

            await ExecuteGetLinksOptimizedIntegrationTest(expectedLinks, StaticSamplePathQueryLinks);
        }

        [Fact]
        public async Task GetLinksOptimized_ReturnsFourOutOfFourLinks_WhenStaticHtmlPageContainingRelativeLinksIsCrawled()
        {
            var expectedLinks = new string[] { "/B/BB/BBB/StaticSampleRelativePathB", "/StaticSampleRelativePathA.html", "StaticSampleRelativeC.html", "StaticSampleRelativeD" };

            await ExecuteGetLinksOptimizedIntegrationTest(expectedLinks, StaticSamplePathRelativeLinks);
        }

        [Fact]
        public async Task GetAllLinks_ReturnsTwentyFourOutOfTwentyFourLinks_WhenEntireWebApplicationIsCrawled()
        {
            var expectedLinks = new string[] {
                "/",
                "/html/StaticSampleAbsolutePath.html",
                "/html/StaticSampleFragment.html",
                "/html/StaticSampleQuery.html",
                "/html/StaticSampleRelativePath.html",
                "/html/StaticSampleSubdomain.html",
                "/WebCrawler/About",
                "/WebCrawler/Sample",
                "StaticSampleSubdomainC.html",
                "test.StaticSampleSubdomainB.html",
                "www.StaticSampleSubdomainA.html",
                "/B/BB/BBB/StaticSampleRelativePathB",
                "/StaticSampleRelativePathA.html",
                "StaticSampleRelativeC.html",
                "StaticSampleRelativeD",
                "/StaticSampleQueryA.html",
                "/StaticSampleQueryB.html",
                "StaticSampleQueryC.html",
                "/StaticSampleFragmentA.html",
                "/StaticSampleFragmentB.html",
                "/StaticSampleFragmentD.html",
                "/B/BB/BBB/StaticSampleAbsolutePathB.html",
                "/StaticSampleAbsolutePathA.html",
                "/StaticSampleAbsolutePathC.html"
            };

            using (var testServer = new TestServer(_builder))
            {
                var client = testServer.CreateClient();

                client.BaseAddress = new Uri(TestServerUrl);

                var linkActions = new Action<string>[expectedLinks.Length];

                for (int i = 0; i < expectedLinks.Length; i++)
                {
                    var expectedLink = expectedLinks[i];

                    linkActions[i] = (a) => { Assert.Equal(expectedLink, a); };
                }

                var links = await WebCrawlerUtility.GetAllLinks(TestServerUrl, 24, client);

                Assert.Collection(links, linkActions);
            }
        }

        private async Task ExecuteGetLinksOptimizedIntegrationTest(string[] expectedLinks, string staticHtmlPageName, string baseServerAddress = TestServerUrl, string requestUriBase = StaticFileDirectoryPath)
        {
            using (var testServer = new TestServer(_builder))
            {
                var client = testServer.CreateClient();

                client.BaseAddress = new Uri(baseServerAddress);

                var linkActions = new Action<string>[expectedLinks.Length];

                for (int i = 0; i < expectedLinks.Length; i++)
                {
                    var expectedLink = expectedLinks[i];

                    linkActions[i] = (a) => { Assert.Equal(expectedLink, a); };
                }

                var links = await WebCrawlerUtility.GetLinksOptimized(client, Path.Combine(requestUriBase, staticHtmlPageName));

                Assert.Collection(links, linkActions);
            }
        }
    }
}

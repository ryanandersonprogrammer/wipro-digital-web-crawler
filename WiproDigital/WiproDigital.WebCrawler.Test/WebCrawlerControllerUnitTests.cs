﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.Extensions.Logging;
using Moq;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using WiproDigital.WebCrawler.Abstractions;
using WiproDigital.WebCrawler.UI.Controllers;
using WiproDigital.WebCrawler.UI.Models;
using Xunit;

namespace WiproDigital.WebCrawler.Test
{
    public class WebCrawlerControllerUnitTests
    {
        private WebCrawlerController _webCrawlerController;

        private Mock<IWebCrawler> _mockWebCrawler;
        private Mock<ILogger<WebCrawlerController>> _mockLogger;

        private const string ValidTestUrl = "http://localhost:5001";
        private const string InvalidTestUrlEmpty = "";
        private const string InvalidTestUrlNull = null;

        private WebCrawlerViewModel _webCrawlerViewModel;

        private HashSet<HashSet<string>> _testLinkGroups;
        private HashSet<string> _testLinks;

        public WebCrawlerControllerUnitTests()
        {
            IWebCrawler webCrawler;

            webCrawler = WebCrawler.Instance;

            var webCrawlerInstanceFieldInfo = typeof(WebCrawler).GetField("_instance", System.Reflection.BindingFlags.Static | System.Reflection.BindingFlags.NonPublic);

            _mockWebCrawler = new Mock<IWebCrawler>();
            _mockLogger = new Mock<ILogger<WebCrawlerController>>();

            webCrawlerInstanceFieldInfo.SetValue(null, new Lazy<IWebCrawler>(_mockWebCrawler.Object));

            _webCrawlerController = new WebCrawlerController(_mockLogger.Object);

            _testLinks = new HashSet<string>();
            _testLinkGroups = new HashSet<HashSet<string>>();

            var testLinks = new HashSet<string>
            {
                "/A.html",
                "/A/AA.html",
                "/A/AA/AAA"
            };

            _testLinks.UnionWith(testLinks);
            _testLinkGroups.Add(testLinks);

            testLinks = new HashSet<string>
            {
                "/B.html",
                "/B/BB.html",
                "/B/BB/BBB"
            };

            _testLinks.UnionWith(testLinks);
            _testLinkGroups.Add(testLinks);

            testLinks = new HashSet<string>
            {
                "/C.html",
                "/C/CC.html",
                "/C/CC/CCC"
            };

            _testLinks.UnionWith(testLinks);
            _testLinkGroups.Add(testLinks);

            _webCrawlerViewModel = new WebCrawlerViewModel
            {
                Url = ValidTestUrl,
                SiteStructureLinkGroups = _testLinkGroups
            };
        }

        [Fact]
        public async Task Crawl_ReturnsViewResultWithIndexViewAndWebCrawlerViewModelContainingExpectedLinks_WhenWebCrawlerViewModelContainingExpectedDataIsPassedAsAnArgument()
        {
            _mockLogger.Setup(x => x.Log(LogLevel.Information, new EventId(), $"Crawl {_webCrawlerViewModel.Url}{Environment.NewLine}", null, It.IsAny<Func<string, Exception, string>>()));
            _mockWebCrawler.Setup(x => x.Crawl(_webCrawlerViewModel.Url, null)).ReturnsAsync(_testLinks);
            _mockWebCrawler.Setup(x => x.GetSiteStructure(_testLinks)).Returns(_testLinkGroups);
            _mockLogger.Setup(x => x.Log(LogLevel.Information, new EventId(), $"Result{Environment.NewLine}", null, It.IsAny<Func<string, Exception, string>>()));
            _mockLogger.Setup(x => x.Log(LogLevel.Information, new EventId(), $"3{Environment.NewLine}", null, It.IsAny<Func<string, Exception, string>>()));

            var httpContext = new DefaultHttpContext();

            var tempDataDictionary = new TempDataDictionary(httpContext, Mock.Of<ITempDataProvider>());

            _webCrawlerController.TempData = tempDataDictionary;

            var viewModel = new WebCrawlerViewModel() { Url = ValidTestUrl };

            var result = (RedirectToActionResult)await _webCrawlerController.Crawl(viewModel);

            Assert.True(_webCrawlerController.TempData.ContainsKey("ViewModel"));

            var actualModel = JsonConvert.DeserializeObject<WebCrawlerViewModel>(_webCrawlerController.TempData["ViewModel"].ToString());

            _mockLogger.Verify(x => x.Log(LogLevel.Information, new EventId(), $"Crawl {_webCrawlerViewModel.Url}{Environment.NewLine}", null, It.IsAny<Func<string, Exception, string>>()), Times.Once);
            _mockWebCrawler.Verify(x => x.Crawl(_webCrawlerViewModel.Url, null), Times.Once);
            _mockWebCrawler.Verify(x => x.GetSiteStructure(_testLinks), Times.Once);
            _mockLogger.Verify(x => x.Log(LogLevel.Information, new EventId(), $"Result{Environment.NewLine}", null, It.IsAny<Func<string, Exception, string>>()), Times.Once);
            _mockLogger.Verify(x => x.Log(LogLevel.Information, new EventId(), $"3{Environment.NewLine}", null, It.IsAny<Func<string, Exception, string>>()), Times.Once);

            var length = _testLinks.Count;

            var linkActions = new Action<string>[length];

            var expectedLinks = new string[length];
            var actualLinks = new List<string>();

            foreach (var linkGroup in _webCrawlerViewModel.SiteStructureLinkGroups)
            {
                foreach (var link in linkGroup)
                {
                    actualLinks.Add(link);
                }
            }

            _testLinks.CopyTo(expectedLinks);

            for (int i = 0; i < length; i++)
            {
                var expectedLink = expectedLinks[i];

                linkActions[i] = (a) => { Assert.Equal(expectedLink, a); };
            }

            Assert.Collection(actualLinks, linkActions);
            Assert.Equal(ValidTestUrl, actualModel.Url);
            Assert.True(TimeSpan.Compare(actualModel.Duration, TimeSpan.Zero) != 0);
            Assert.True((bool)_webCrawlerController.ViewData["Visible"]);
            Assert.Equal("Index", result.ActionName);
        }

        [Fact]
        public async Task Crawl_ReturnsViewResultWithIndexViewAndWebCrawlerViewModelWithoutLinks_WhenWebCrawlerViewModelContainingEmptyUrlIsPassedAsAnArgument()
        {
            _mockWebCrawler.Setup(x => x.Crawl(_webCrawlerViewModel.Url, null)).ReturnsAsync(_testLinks);
            _mockWebCrawler.Setup(x => x.GetSiteStructure(_testLinks)).Returns(_testLinkGroups);
            _mockLogger.Setup(x => x.Log(LogLevel.Error, new EventId(), "The URL is invalid.", null, It.IsAny<Func<string, Exception, string>>()));

            var viewModel = new WebCrawlerViewModel() { Url = InvalidTestUrlEmpty };

            var result = (ViewResult)await _webCrawlerController.Crawl(viewModel);

            var actualModel = (WebCrawlerViewModel)result.Model;

            _mockWebCrawler.Verify(x => x.Crawl(_webCrawlerViewModel.Url, null), Times.Never);
            _mockWebCrawler.Verify(x => x.GetSiteStructure(_testLinks), Times.Never);
            _mockLogger.Verify(x => x.Log(LogLevel.Error, new EventId(), "The URL is invalid.", null, It.IsAny<Func<string, Exception, string>>()));

            Assert.Equal(string.Empty, actualModel.Url);
            Assert.Null(actualModel.SiteStructureLinkGroups);
            Assert.Null(actualModel.PageIndex);
            Assert.True(TimeSpan.Compare(actualModel.Duration, TimeSpan.Zero) == 0);
            Assert.False((bool)result.ViewData["Visible"]);
            Assert.Equal("Index", result.ViewName);
        }

        [Fact]
        public async Task Crawl_ReturnsViewResultWithIndexViewAndWebCrawlerViewModelWithoutLinks_WhenWebCrawlerViewModelContainingNullUrlIsPassedAsAnArgument()
        {
            _mockWebCrawler.Setup(x => x.Crawl(_webCrawlerViewModel.Url, null)).ReturnsAsync(_testLinks);
            _mockWebCrawler.Setup(x => x.GetSiteStructure(_testLinks)).Returns(_testLinkGroups);
            _mockLogger.Setup(x => x.Log(LogLevel.Error, new EventId(), "The URL is invalid.", null, It.IsAny<Func<string, Exception, string>>()));

            var viewModel = new WebCrawlerViewModel() { Url = InvalidTestUrlNull };

            var result = (ViewResult)await _webCrawlerController.Crawl(viewModel);

            var actualModel = (WebCrawlerViewModel)result.Model;

            _mockWebCrawler.Verify(x => x.Crawl(_webCrawlerViewModel.Url, null), Times.Never);
            _mockWebCrawler.Verify(x => x.GetSiteStructure(_testLinks), Times.Never);
            _mockLogger.Verify(x => x.Log(LogLevel.Error, new EventId(), "The URL is invalid.", null, It.IsAny<Func<string, Exception, string>>()));

            Assert.Null(actualModel.Url);
            Assert.Null(actualModel.SiteStructureLinkGroups);
            Assert.Null(actualModel.PageIndex);
            Assert.True(TimeSpan.Compare(actualModel.Duration, TimeSpan.Zero) == 0);
            Assert.False((bool)result.ViewData["Visible"]);
            Assert.Equal("Index", result.ViewName);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace WiproDigital.WebCrawler.UI.Models
{
    public class WebCrawlerViewModel
    {
        [Required]
        public string Url { get; set; }

        public TimeSpan Duration { get; set; }

        public HashSet<HashSet<string>> SiteStructureLinkGroups { get; set; }

        public int? PageIndex { get; set; }
    }
}

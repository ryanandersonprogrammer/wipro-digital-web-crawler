﻿using Microsoft.AspNetCore.Mvc;
using WiproDigital.WebCrawler.UI.Control;

namespace WiproDigital.WebCrawler.UI.ViewComponents
{
    public class PaginationViewModel
    {
        public PaginationViewModel(Pagination pagination)
        {
            Pagination = pagination;
        }

        public Pagination Pagination { get; }
    }

    [ViewComponent(Name = "Pagination")]
    public class PaginationViewComponent : ViewComponent
    {
        public IViewComponentResult Invoke(PaginationViewModel paginationViewModel)
        {
            return View(paginationViewModel);
        }
    }
}

﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using WiproDigital.WebCrawler.Abstractions;
using WiproDigital.WebCrawler.UI.Models;

namespace WiproDigital.WebCrawler.UI.Controllers
{
    public class WebCrawlerController : Controller
    {
        private readonly IWebCrawler _webCrawler;

        private readonly ILogger<WebCrawlerController> _logger;

        public WebCrawlerController(ILogger<WebCrawlerController> logger)
        {
            _webCrawler = WebCrawler.Instance;

            _logger = logger;
        }

        [HttpGet]
        public IActionResult Index(WebCrawlerViewModel webCrawlerViewModel, int? page = 1)
        {
            if (!ModelState.IsValid)
                ModelState.Clear();

            if (TempData.ContainsKey("ViewModel") && TempData["ViewModel"] != null)
            {
                webCrawlerViewModel = JsonConvert.DeserializeObject<WebCrawlerViewModel>(TempData["ViewModel"].ToString());

                TempData.Keep("ViewModel");
            }

            if (webCrawlerViewModel == null)
                webCrawlerViewModel = new WebCrawlerViewModel();

            ViewData["Visible"] = true;

            if (webCrawlerViewModel.SiteStructureLinkGroups == null || webCrawlerViewModel.SiteStructureLinkGroups.Count == 0)
                ViewData["Visible"] = false;

            webCrawlerViewModel.PageIndex = page;

            return View(webCrawlerViewModel);
        }

        [HttpPost]
        public async Task<IActionResult> Crawl(WebCrawlerViewModel webCrawlerViewModel)
        {
            _logger.Log(LogLevel.Information, new EventId(), $"Crawl {webCrawlerViewModel.Url}{Environment.NewLine}", null, (s, e) => { return null; });

            ViewData["Visible"] = true;

            if (!string.IsNullOrWhiteSpace(webCrawlerViewModel.Url))
            {
                var stopWatch = new Stopwatch();

                stopWatch.Start();

                var links = await _webCrawler.Crawl(webCrawlerViewModel.Url);

                stopWatch.Stop();

                if (links.Count > 0)
                {
                    webCrawlerViewModel.SiteStructureLinkGroups = _webCrawler.GetSiteStructure(links);
                    webCrawlerViewModel.Duration = stopWatch.Elapsed;

                    _logger.Log(LogLevel.Information, new EventId(), $"Result{Environment.NewLine}", null, (s, e) => { return null; });
                    _logger.Log(LogLevel.Information, new EventId(), $"{webCrawlerViewModel.SiteStructureLinkGroups.Count}{Environment.NewLine}", null, (s, e) => { return null; });
                }
            }
            else
                _logger.Log(LogLevel.Error, new EventId(), "The URL is invalid.", null, (s, e) => { return null; });

            if (webCrawlerViewModel.SiteStructureLinkGroups == null || webCrawlerViewModel.SiteStructureLinkGroups.Count == 0)
            {
                ViewData["Visible"] = false;

                return View("Index", webCrawlerViewModel);
            }

            TempData["ViewModel"] = JsonConvert.SerializeObject(webCrawlerViewModel);

            return RedirectToAction("Index");
        }

        [HttpGet]
        public IActionResult About()
        {
            return View();
        }

        [HttpGet]
        public IActionResult Sample()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}

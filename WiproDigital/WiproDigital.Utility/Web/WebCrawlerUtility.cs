﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace WiproDigital.WebCrawler.Utility.Web
{
    /// <summary>
    /// This utility class contains methods for crawling websites.
    /// </summary>
    public static class WebCrawlerUtility
    {
        /// <summary>
        /// This method can be used to collect all of the links from the first site at a URL address that is set in an instance of an HTTP client.
        /// </summary>
        /// <param name="httpClient">This is an HTTP client that sends an HTTP GET request to retrieve all of the content from a site.</param>
        /// <param name="requestUri">This is a request path that gets appended to the base server address that is set in an instance of the HTTP client.</param>
        /// <returns>This method returns a hash set containing links.</returns>
        public static async Task<HashSet<string>> GetLinksOptimized(HttpClient httpClient, string requestUri)
        {
            var urlUri = $"{httpClient.BaseAddress.Scheme}{Uri.SchemeDelimiter}{httpClient.BaseAddress.Authority}";
            var domainToken = httpClient.BaseAddress.Authority;
            var links = new HashSet<string>(StringComparer.OrdinalIgnoreCase); // Use a hash set to easily prevent duplication.

            string pageContent;

            try
            {
                pageContent = await httpClient.GetStringAsync(requestUri);
            }
            catch (Exception)
            {
                return links;
            }

            var document = new HtmlDocument();

            document.LoadHtml(pageContent);

            var nodes = document.DocumentNode
                .SelectNodes("//a[@href]");

            if (nodes == null)
                return links;

            var nodeValues = nodes
                .GroupBy(x => x.GetAttributeValue("href", string.Empty))
                .Select(x => x.First().GetAttributeValue("href", string.Empty))
                .ToList();

            nodeValues.Sort();

            for (int i = 0; i < nodeValues.Count(); i++)
            {
                var hrefValue = nodeValues[i].Trim();

                if (hrefValue.StartsWith("mailto")) // Exclude any links that contain a mailto scheme.
                    continue;

                if (hrefValue.Contains('#')) // Remove fragments.
                {
                    var fragmentIndex = hrefValue.IndexOf('#');

                    if (fragmentIndex > 0 && hrefValue[fragmentIndex - 1].Equals('/'))
                        fragmentIndex--;

                    hrefValue = hrefValue.Remove(fragmentIndex, hrefValue.Count() - fragmentIndex);
                }

                if (hrefValue.Contains('?')) // Remove queries.
                {
                    var queryIndex = hrefValue.IndexOf('?');

                    if (queryIndex > 0 && hrefValue[queryIndex - 1].Equals('/'))
                        queryIndex--;

                    hrefValue = hrefValue.Remove(queryIndex, hrefValue.Count() - queryIndex);
                }

                if (string.IsNullOrWhiteSpace(hrefValue))
                    continue;

                if (!Uri.TryCreate(hrefValue, UriKind.RelativeOrAbsolute, out Uri hrefUri))
                    continue;

                if (hrefUri.IsAbsoluteUri) // Determine whether the absolute path contains the given domain name.
                {
                    var domains = hrefUri.Authority.Split('.');
                    var hasSubdomain = domains.Count() > 2 && domains[0] != "www";

                    if (!hrefValue.Contains(domainToken) || hasSubdomain) // Exclude URLs that contain domains of external websites and subdomains other than www.
                        continue;

                    hrefValue = hrefValue.Replace($"{hrefUri.Scheme}{Uri.SchemeDelimiter}{hrefUri.Authority}", string.Empty);
                }

                if (string.IsNullOrWhiteSpace(hrefValue) || hrefValue.Equals('/'))
                    continue;

                links.Add(hrefValue);
            }

            return links;
        }

        /// <summary>
        /// This method can be used to collect all top-level links from a website.
        /// </summary>
        /// <param name="url">This is the base server address of a website.</param>
        /// <returns>This method returns a hash set containing all links from a website.</returns>
        public static async Task<HashSet<string>> GetAllLinksBasic(string url)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(url);

                var initialHrefValues = await GetLinksOptimized(client, string.Empty);
                var links = new HashSet<string>(initialHrefValues);

                foreach (string hrefValue in initialHrefValues)
                    links.UnionWith(await GetLinksOptimized(client, hrefValue));

                return links;
            }
        }

        /// <summary>
        /// This method can be used to collect all links from a website.
        /// </summary>
        /// <param name="url">This is the base server address of a website.</param>
        /// <param name="maximumNumberOfLinks">This is the maximum number of links that can be collected by this operation.</param>
        /// <returns>This method returns a hash set containing all links from a website.</returns>
        public static async Task<HashSet<string>> GetAllLinks(string url, int maximumNumberOfLinks)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(url);

                return await GetAllLinksHelper(maximumNumberOfLinks, client);
            }
        }

        /// <summary>
        /// This method can be used to collect all links from a website.
        /// </summary>
        /// <param name="url">This is the base server address of a website.</param>
        /// <param name="maximumNumberOfLinks">This is the maximum number of links that can be collected by this operation.</param>
        /// <param name="client">This is an HTTP client; if this value is null, then the provided URL will be used as a base server address of an internal client.</param>
        /// <returns>This method returns a hash set containing all links from a website.</returns>
        public static async Task<HashSet<string>> GetAllLinks(string url, int maximumNumberOfLinks, HttpClient client)
        {
            if (client == null)
                return await GetAllLinks(url, maximumNumberOfLinks);

            return await GetAllLinksHelper(maximumNumberOfLinks, client);
        }

        private static async Task<HashSet<string>> GetAllLinksHelper(int maximumNumberOfLinks, HttpClient client)
        {
            var initialHrefValues = await GetLinksOptimized(client, string.Empty);
            var linkStack = new Stack<string>(initialHrefValues);
            var links = new HashSet<string>();

            while (linkStack.Any())
            {
                var next = linkStack.Pop();

                var childrenLinks = await GetLinksOptimized(client, next);

                foreach (var child in childrenLinks)
                {
                    if (!links.Contains(child))
                        linkStack.Push(child);

                    links.Add(child);

                    if (links.Count == maximumNumberOfLinks)
                        return links;
                }
            }

            return links;
        }
    }
}
